let $num1 = $('#1');
let $num2 = $('#2');
let $num3 = $('#3');
let $num4 = $('#4');
let $num5 = $('#5');
let $num6 = $('#6');
let $num7 = $('#7');
let $num8 = $('#8');
let $num9 = $('#9');
let $equal = $('#equal');
let $decimalPoint = $('#decimalPoint');
let $CE = $('#CE');
let $C = $('#C');

let $nums = $('.num');
let $bottomTextField = $('#bottomTextField');
let $upperTextField = $('#upperTextField');
let $operations = $('.operation'); // +-*/

let result = '0';
let keepResult = '';
let updateAfterOperationClicked = false; // when operation is clicked, the text from bottom field is moved to upper text field, thats why we are doing this
let normalNumUpdating = true; // when operation is clicked, the text from bottom field is moved to upper text field, thats why we are doing this
let clearEntry = false;

// call this method when number is clicked
let updateWhenNumClicked = (numEventClick) => {
    let value = numEventClick.target.innerText;
    if (result === '0') {
        result = '';
    }

    // when operation is clicked, the text from bottom field is moved to upper text field, thats why we are doing this
    if (updateAfterOperationClicked) {
        result = '';
        result += value;
        $bottomTextField.val(result);
        updateAfterOperationClicked = false;
        normalNumUpdating = true;
    } else if (normalNumUpdating) {
        result += value;
        $bottomTextField.val(result);
    }
}

// call this when operation (+-/*) is clicked
let updateWhenOperationClicked = (operationEventClick) => {
    let operation = operationEventClick.target.innerText;

    // to keep information from upper text field when is clicked CE
    if (clearEntry) {
        $upperTextField.text(keepResult + ' ' + operation);
        result = keepResult;
        clearEntry = false;
    } else {
        $upperTextField.text(result + ' ' + operation);
    }    

    updateAfterOperationClicked = true;
    normalNumUpdating = false;
}

for (let i = 0; i < $nums.length; i++) {
    $nums[i].addEventListener('click', updateWhenNumClicked, false);
}

for (let i = 0; i < $operations.length; i++) {
    $operations[i].addEventListener('click', updateWhenOperationClicked, false);
}

$equal[0].onclick = () => {
    let firstNum = Number($upperTextField.text().slice(0, $upperTextField.text().length - 2));
    let secondNum = Number($bottomTextField.val());
    let operation = $upperTextField.text().substring($upperTextField.text().length - 2);

    switch (operation.trim()) {
        case "X": {
            result = firstNum * secondNum;
            break;   
        }
        case "/": {
            if (secondNum == 0) {
                result = 'cannot divide by zero';
            } else {
                result = firstNum / secondNum;
            }            
            break;
        }
        case "-": {
            result = firstNum - secondNum;
            break;
        }
        case "+": {
            result = firstNum + secondNum;
            break;
        }
    }
    $upperTextField.text('');
    $bottomTextField.val('');
    $bottomTextField.val(result);
}

$decimalPoint[0].onclick = () => {
    // add only 1 dot
    if (!$bottomTextField.val().includes('.')) {
        $bottomTextField.val($bottomTextField.val() + '.');
        result += '.';
    }    
}

$CE[0].onclick = () => {
    keepResult = result; // to keep information from upper text field when is clicked CE
    $bottomTextField.val('0');
    result = '0';
    clearEntry = true; // to keep information from upper text field when is clicked CE
}

$C[0].onclick = () => {
    $bottomTextField.val('0');
    result = '0';
    $upperTextField.text('');
}
